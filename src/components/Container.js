import React from 'react';
import styled from 'styled-components';
import { Text, Image } from 'react-native'

class Container extends React.PureComponent {
    render(){
        const { product } = this.props;
        return (
            <ContainerWrapper>
								<Image
									style={{width: 100, height: 100}}
									source={{uri: product.imagens}}
        				/>
                <Text> Titulo: {product.titulo} </Text>
                <Text> Preço: {product.preço} </Text>
            </ContainerWrapper>
        );
    }
}

const ContainerWrapper = styled.View`
	background-color: #9ACD32;
  flex-direction: column;
	margin: 8px;
	padding: 5px;
	border-radius:10px
`;


export default Container;